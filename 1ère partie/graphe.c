#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>

int main(int argc, char *argv[])
{

    // Verifier la bonne utilisation du programme ./client adresse_ip_serveur port_serveur port_client
    if (argc != 6)
    {
        printf("utilisation : %s ip_serveur port_serveur ton_indice ton_port ton_ip\n", argv[0]);
        exit(1);
    }

    /*==========================================COMMUNICATION AVEC SERVEUR CENTRAL ============================================================*/
    printf("*******Je commence l'échange avec le serveur annuaire*******\n");
    /* Etape 1 : créer une socket avec le type stream utilise le protocole UDP */
    int socketClient = socket(PF_INET, SOCK_DGRAM, 0);

    // verifier la bonne création
    if (socketClient == -1)
    {
        perror("Client : problème dans la creation de socket client :");
        // exit(1);
    }

    /* Etape 2 : Nommer la socket du client */
    struct sockaddr_in ad;
    ad.sin_family = AF_INET;
    inet_pton(AF_INET, argv[5], &(ad.sin_addr));
    ad.sin_port = htons((short)atoi(argv[4]));
    int nom = bind(socketClient, (struct sockaddr *)&ad, sizeof(ad));

    if (nom == -1)
    {
        perror("Client : erreur au nommage de la socket \n");
    }

    /* Etape 3 : Désigner la socket du serveur */
    struct sockaddr_in sockServ;
    sockServ.sin_family = AF_INET;
    inet_pton(AF_INET, argv[1], &(sockServ.sin_addr));
    sockServ.sin_port = htons((short)atoi(argv[2]));
    socklen_t lgA = sizeof(struct sockaddr_in);

    /* Etape 4 :Envoyer un message au serveur */

    int message = atoi(argv[3]);
    int envoi = sendto(socketClient, &message, sizeof(int), 0, (struct sockaddr *)&sockServ, lgA);
    if (envoi == -1)
    {
        perror("Client : probleme lors de l'envoi du message\n ");
        exit(1);
    }
    else if (envoi == 0)
    {
        perror("Client : la socket a été fermée \n ");
        exit(1);
    }
    printf("Noeud %i: ma socket a été envoyé au serveur annuaire!\n ", atoi(argv[3]));

    int taille = 0;
    int receptionTaille = recvfrom(socketClient, &taille, sizeof(int), 0, (struct sockaddr *)&sockServ, &lgA);
    while (receptionTaille == -1)
    {
        perror("Client : probleme lors de la réception du message\n ");
        exit(1);
    }
    // printf("taille du tableau des voisins auxquels je dois me connecter : %i\n", taille);

    struct sockaddr_in voisins[taille];

    for (int i = 0; i < taille; i++)
    {
        int receptionTableau = recvfrom(socketClient, &voisins[i], sizeof(struct sockaddr_in), 0, (struct sockaddr *)&sockServ, &lgA);
        while (receptionTableau == -1)
        {
            perror("Client : probleme lors de la réception du message\n ");
            exit(1);
        }
    }
    int cpt = 0;
    int receptionNbClients = recvfrom(socketClient, &cpt, sizeof(int), 0, (struct sockaddr *)&sockServ, &lgA);
    while (receptionNbClients == -1)
    {
        perror("Client : probleme lors de la réception du message\n ");
        exit(1);
    }
    /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
    close(socketClient);
    // shutdown(socketClient,0);
    printf("*******Je termine l'échange avec le serveur annuaire*******\n");

    //----------------------------------------------------------------Quand je suis serveur je me mets en écoute-----------------------------------------------------------------------
    /* Etape 1 : créer une socket */
    int socketServer = socket(PF_INET, SOCK_STREAM, 0); // SOCK_STREAM pour le TCP

    if (socketServer == -1)
    {
        perror("Serveur : pb creation socket :");
    }

    // printf("Serveur : creation de la socket réussie \n");

    /* Etape 2 : Nommer ma socket entant que seveur */
    struct sockaddr_in addrServer;
    addrServer.sin_family = AF_INET;
    inet_pton(AF_INET, argv[5], &(addrServer.sin_addr));
    addrServer.sin_port = htons((short)atoi(argv[4]));

    int res = bind(socketServer, (const struct sockaddr *)&addrServer, sizeof(addrServer));

    if (res == -1)
    {
        perror("Serveur :problème lors du nommage de la socket\n ");
        // exit(1);
    }

    // printf("Socket du serveur bien nommée \n");
    sleep(3);
    /*Etape 3:passer la socket en mode écoute */
    int ecoute = listen(socketServer, cpt);
    if (ecoute == -1)
    {
        perror("Serveur TCP :erreur lors de la mise sur écoute \n");
        // exit(1);
    }
    // printf("le serveur a été mis sur ecoute avec succes \n ");

    //------------------------------------------- Quand je suis client---------------------------------------------------------------------------------------------
    for (int i = 0; i < taille; i++)
    {

        // Le passage de paramètre est fortement conseillé
        int indice = atoi(argv[3]);
        int portServeur = ntohs(voisins[i].sin_port);
        char *adresseIPServeur = inet_ntoa(voisins[i].sin_addr);
        // printf("Client : je veux me connecter au serveur %i (ip : %s:port %d)\n", (ntohs(voisins[i].sin_port) - 3000), inet_ntoa(voisins[i].sin_addr), ntohs(voisins[i].sin_port));

        int socketClient = socket(PF_INET, SOCK_STREAM, 0);

        // verifier la bonne création
        if (socketClient == -1)
        {
            perror("Client : problème dans la creation de socket client :");
        }

        // printf("Client : creation de la socket réussie \n");

        /* Etape 3 : Désigner la socket du serveur */
        struct sockaddr_in sockServ;
        sockServ.sin_family = AF_INET;
        // inet_pton(AF_INET,args->adresseIPServeur,&(sockServ.sin_addr));
        sockServ.sin_addr.s_addr = inet_addr(adresseIPServeur);
        sockServ.sin_port = htons(portServeur);
        socklen_t lgA = sizeof(struct sockaddr_in);
        // demande de connexion
        int res;
        while ((res = connect(socketClient, (struct sockaddr *)&sockServ, lgA)) == -1)
        {
            sleep(2);
        }
        // printf("Client : la demande de connexion a bien été envoyé au voisin \n");

        /* Etape 4 :Envoyer un message au serveur */
        int envoi;
        envoi = send(socketClient, &indice, sizeof(int), 0);
        if (envoi == -1)
        {
            perror("Client : probleme lors de l'envoi du message\n ");
        }
        else if (envoi == 0)
        {
            perror("Client : la socket a été fermée \n ");
        }
        printf("Noeud %i : je me suis connecté & j'ai envoyé un message à mon voisin %i!\n ", atoi(argv[3]), (portServeur - 3000));
        close(socketClient);
    }
    //------------------------------------------- Quand je suis serveur j'accepte et je reçois---------------------------------------------------------------------------------------------
    struct sockaddr_in adClient;
    socklen_t lgAA = sizeof(adClient);
    int dsClient;

    fd_set set, settmp;         // set contiendra toutes les sockets à scruter, settmp est une copie de set modifiable par select
    FD_ZERO(&set);              /// Initialise à faux les elements de l'ensemble pointé par set.
    FD_SET(socketServer, &set); // Ajoute le descripteur socketServer à la liste des descripteurs de *set à scruter, i.e. positionne l'éléement à l'indice desc à vrai.
    int max = socketServer;
    while (1)
    {
        settmp = set;
        // je scrute en lecture, rien n'empeche de le faire aussi en ecriture
        select(max + 1, // descripteur max à scruter +1
               &settmp, // descripteurs à scruter en entree (readDescs)
               NULL,    // descripteurs à scruter en sortie (writeDescs)
               NULL,    // pour scruter des exceptions
               NULL);   // délais d'attente ou NULL // Si le paramètre timeout est NULL, la fonction attend jusqu'à l'occurrence d'un événement,
        for (int df = 2; df <= max; df++)
        {
            if (!FD_ISSET(df, &settmp)) // Teste si le descripteur socketServer est dans la liste des descripteurs de *set à scruter ou scrutés, i.e. si l'élément à l'indice desc est à vrai.
                continue;
            struct sockaddr_in adCv;
            socklen_t lgCv = sizeof(struct sockaddr_in);
            int dsCv;
            if (df == socketServer)
            {
                // printf("Serveur : j'ai reçu une demande de connexion \n");

                dsCv = accept(socketServer, (struct sockaddr *)&adCv, &lgCv);
                if (dsCv < 0)
                {
                    perror("Serveur, pb accept :");
                    exit(1);
                }
                else
                {
                    // printf("Noeud %i : le client (ip %s: port %d) est connecte  \n",atoi(argv[3]),inet_ntoa(adCv.sin_addr),ntohs(adCv.sin_port));
                    FD_SET(dsCv, &set); // ajouter la socket à l'ensemble des sockets à scruter
                    if (max < dsCv)
                        max = dsCv;
                    int msg = 0;
                    int reception = recv(dsCv, &msg, sizeof(int), 0);

                    while (reception == -1)
                    {
                        sleep(1);
                    }
                    printf("Noeud %i : connexion réussie & message reçu du noeud %i(ip %s: port %d)\n", atoi(argv[3]), msg, inet_ntoa(adCv.sin_addr), ntohs(adCv.sin_port));
                    continue;
                }
            }
        }
    }
    getchar();
}
