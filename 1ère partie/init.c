#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("Utilisation : %s nomDuFichier\n", argv[0]);
        exit(1);
    }

    int exit_status;
    FILE *fichier;
    char ligne[500];

    char EdgeStr[10];
    int sommets;
    int aretes;
    int sommet1;
    int sommet2;

    fichier = fopen(argv[1], "r");
    if (fichier == NULL)
        return 1;

    char lineType = 'c';
    // lire les lignes de commentaires jusqu'au premier 'p'
    while (lineType == 'c')
    {
        fgets(ligne, 500, fichier); //
        lineType = ligne[0];
    }
    // récupération des paramètres de la ligne 'p'
    if (ligne[0] == 'p')
    { //ça sert surtout à récupérer le nombre de sommets pour pouvoir exécuter le programme graphe nbSommets fois
        sscanf(ligne, "%c %s %d %d\n", &lineType, EdgeStr, &sommets, &aretes);
        printf("%c %s %d %d\n", lineType, EdgeStr, sommets, aretes);
    }
    char buf[100];
    for (int i = 0; i < sommets; i++)
    { // pour chaque sommet je fais cette instruction
        // je stocke cette chaine de caractères dans buff pour la passer à system après, gnome-terminal pour créer à chaque fois un terminal
        sprintf(buf, "gnome-terminal -- ./graphe 127.0.0.1 4000 %i 300%i 127.0.0.1", i+1, i+1);
        printf("%i \n", i);
        exit_status = system(buf); // je la passe à system
        if (exit_status == -1)
        {
            perror("Failed opening terminal\n");
            exit(1);
        }
    }
}
