#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

#ifndef FONCTIONS_H
#define FONCTIONS_H

int MAX_COLORS;

struct Message
{
    int sender_id;   // l'id du processus
    int timestamp;   // estmapille du message
    bool is_request; // si c'est de type REQUEST
    bool is_reply;   // si c'est de type REPLY
    bool is_release; // si c'est de type RELEASE
    int couleur;     // si c'est un RELEASE on envoie la couleur choisie avec >1
};

struct variableCommune
{
    int *indice_voisins; // tableau qui contient les numéro des sommets de tous les voisins
    int rep_attendues;   // réponses attendues pour entrer en SC
    int *colors;         // tableau des couleurs choisies par les voisins
    bool isRentre;       // si je suis rentré en SC, cette variable est utilisée pour ne pas rentrer en SC dans les 2 threads après l'attente
    int couleur;         // la couleur qui s'attribue le noeud pour qu'une fois il sort il l'envoie à tous les voisins avec les threads (soit il est serveur donc il l'envoie à ses clients, soit il est client donc il l'envoie à ses voisins serveurs)
};

struct node
{
    int my_id;                                      // Identifiant unique du processus courant
    int horloge;                                    // horloge du processus qui sera mise à jour au fur et à mesur
    bool demandeur;                                 // si le noeud est demandeur ou pas
    int nombre_voisins_auxquels_je_me_connecte;     // Nombre de voisins du nœud auxquels il va se connecter en tant que client (quand il est à droite du tableau)
    int nombre_voisins_qui_vont_se_connecter_a_moi; // Nombre de voisins du nœud qui vont se connecter à lui en tant que serveur
    int nombre_voisins;                             // nbr voisins total
    struct sockaddr_in *voisins;                    // Liste des sockets de ses voisins qui va se connecter à eux
    bool *queue_request;                            // la queue de requêtes en attente (file d'attente - tableau differe dans le TD)
    int heure_demande;                              // l'horloge de l'heure de la demande de la SC
    int mon_port;                                   // port
    char *mon_adresseIP;                            // adresse IP
    struct Message *message_a_envoyer;              // la struct du message à envoyer
    pthread_mutex_t *mutex_fonction;
    pthread_mutex_t *mutex_var_partagees;
    pthread_mutex_t *mutex_section_critique; // des mutex chacun a sa fonctionnalité
    pthread_mutex_t *mutex_liberation;
    pthread_mutex_t *mutex_nombre;
    pthread_cond_t *cond;               // la variable conditionnelle
    struct variableCommune *varCommune; // les var partagées entre les 2 threads
};

bool isColorAvailable(struct node *node, int color)
{
    // Check si la couleur donnée est utilisée par un autre voisin
    for (int i = 0; i < node->nombre_voisins; i++)
    {
        if (node->varCommune->colors[i] == color)
        {
            return false;
        }
    }
    return true;
}
// la fonction pour s'attribuer une couleur
int acquisition(struct node *node)
{
    while (true)
    {
        for (int c = 1; c <= MAX_COLORS; c++)
        {
            if (isColorAvailable(node, c)) // si la couleur est dispo
            {
                return c; // je la prends
            }
        }
        MAX_COLORS++; // sinon j'incrémente max_colors de +1 et je la prends
    }
}

void *reception(void *params)
{
    struct node *arg = (struct node *)params;
    // je lock
    pthread_mutex_lock(arg->mutex_fonction);
    if (!arg->nombre_voisins_qui_vont_se_connecter_a_moi == 0)
    {
        int socketServer = socket(PF_INET, SOCK_STREAM, 0); // SOCK_STREAM pour le TCP
        if (socketServer == -1)
        {
            perror("Serveur : pb creation socket :");
        }

        /* Etape 2 : Nommer ma socket entant que seveur */
        struct sockaddr_in addrServer;
        addrServer.sin_family = AF_INET;
        inet_pton(AF_INET, arg->mon_adresseIP, &(addrServer.sin_addr));
        addrServer.sin_port = htons(arg->mon_port);
        int res;

        while (res = bind(socketServer, (const struct sockaddr *)&addrServer, sizeof(addrServer)) == -1)
        {
            perror("erreur nommage\n");
            exit(1);
        }
        /*Etape 3:passer la socket en mode écoute */
        int ecoute = listen(socketServer, arg->nombre_voisins_qui_vont_se_connecter_a_moi);
        if (ecoute == -1)
        {
            perror("Serveur TCP :erreur lors de la mise sur écoute \n");
            exit(1);
        }
        struct sockaddr_in adClient;
        socklen_t lgAA = sizeof(adClient);
        int dsClient;
        fd_set set, settmp;         // set contiendra toutes les sockets à scruter, settmp est une copie de set modifiable par select
        FD_ZERO(&set);              /// Initialise à faux les elements de l'ensemble pointé par set.
        FD_SET(socketServer, &set); // Ajoute le descripteur socketServer à la liste des descripteurs de *set à scruter, i.e. positionne l'éléement à l'indice desc à vrai.
        int max = socketServer;
        int k = 0;
        int msg = 0;
        // pour pouvoir stocker les desripteurs fichiers des clients qui vont se connecter
        int *dsCv = (int *)malloc((arg->nombre_voisins_qui_vont_se_connecter_a_moi) * sizeof(int));
        int nombre = 0;
        // tant que j'ai pas reçu toutes les REPLY (la variable nombre sert à ça, elle s'incrémente une fois qu'un REPLY (ou RELEASE) est reçu)
        while (nombre < arg->nombre_voisins_qui_vont_se_connecter_a_moi)
        {
            settmp = set;
            // je scrute en lecture, rien n'empeche de le faire aussi en ecriture
            select(max + 1, // descripteur max à scruter +1
                   &settmp, // descripteurs à scruter en entree (readDescs)
                   NULL,    // descripteurs à scruter en sortie (writeDescs)
                   NULL,    // pour scruter des exceptions
                   NULL);   // délais d'attente ou NULL // Si le paramètre timeout est NULL, la fonction attend jusqu'à l'occurrence d'un événement,
            for (int df = 2; df <= max; df++)
            {
                if (!FD_ISSET(df, &settmp)) // Teste si le descripteur socketServer est dans la liste des descripteurs de *set à scruter ou scrutés, i.e. si l'élément à l'indice desc est à vrai.
                    continue;
                struct sockaddr_in adCv;
                socklen_t lgCv = sizeof(struct sockaddr_in);
                if (df == socketServer)
                { // je l'accepte et je stocke son fd
                    dsCv[k] = accept(socketServer, (struct sockaddr *)&adCv, &lgCv);
                    if (dsCv[k] < 0)
                    {
                        perror("Serveur, pb accept :");
                        exit(1);
                    }
                    else
                    {
                        FD_SET(dsCv[k], &set); // ajouter la socket à l'ensemble des sockets à scruter
                        if (max < dsCv[k])
                            max = dsCv[k];
                        int reception;
                        while (reception = recv(dsCv[k], &msg, sizeof(int), 0) == -1) // tant que j'ai pas pu recevoir je boucle jusqu'à ce que je reçois
                        {
                            reception = recv(dsCv[k], &msg, sizeof(int), 0);
                        }
                        printf("Connexion réussie avec %i\n", msg);
                        pthread_mutex_lock(arg->mutex_var_partagees);
                        arg->varCommune->indice_voisins[k] = msg;
                        pthread_mutex_unlock(arg->mutex_var_partagees);
                        // Incrementer mon horloge et envoyer un message REQUETE à tous les voisins
                        time_t currentTime;
                        arg->horloge = time(&currentTime);
                        arg->horloge++;
                        arg->heure_demande = arg->horloge;
                        arg->demandeur = true;
                        // le message à envoyer
                        struct Message message_a_envoyer;
                        message_a_envoyer.sender_id = arg->my_id;
                        message_a_envoyer.is_request = true;
                        message_a_envoyer.is_reply = false;
                        message_a_envoyer.timestamp = arg->heure_demande;
                        // send REQUEST TCP
                        int envoi_REQUEST = send(dsCv[k], &message_a_envoyer, sizeof(struct Message), 0);
                        if (envoi_REQUEST == -1)
                        {
                            perror("Client : probleme lors de l'envoi du message\n ");
                            exit(1);
                        }
                        else if (envoi_REQUEST == 0)
                        {
                            perror("Client : la socket a été fermée \n ");
                            exit(1);
                        }
                        printf("REQUEST envoyée au %i\n", arg->varCommune->indice_voisins[k]);
                        int reception2;
                        // pour recevoir le message
                        struct Message msg_recu;
                        while (reception2 = recv(dsCv[k], &msg_recu, sizeof(struct Message), 0) == -1)
                        {
                            reception2 = recv(dsCv[k], &msg_recu, sizeof(struct Message), 0);
                        }
                        printf("REQUEST reçue du %i\n", msg_recu.sender_id);
                        // mettre à jour mon horloge
                        arg->horloge = fmax(arg->horloge, msg_recu.timestamp);
                        // si je suis pas prioritaire je lui envoie une réponse REPLY pour qu'il rentre dans la sec crit
                        if (!arg->demandeur || (arg->heure_demande > msg_recu.timestamp) || ((arg->heure_demande == msg_recu.timestamp) && (arg->my_id > msg_recu.sender_id)))
                        {
                            message_a_envoyer.sender_id = arg->my_id;
                            message_a_envoyer.is_request = false;
                            message_a_envoyer.is_reply = true;

                            int envoi_REPLY2 = send(dsCv[k], &message_a_envoyer, sizeof(struct Message), 0);
                            printf("REPLY envoyé au %i\n", arg->varCommune->indice_voisins[k]);
                            if (envoi_REPLY2 == -1)
                            {
                                perror("Client : probleme lors de l'envoi du message REPLY\n ");
                                exit(1);
                            }
                            else if (envoi_REPLY2 == 0)
                            {
                                perror("Client : la socket a été fermée \n ");
                                exit(1);
                            }
                            // je clear le message de REQUEST reçu et la REQUESt envoyée
                            memset(&message_a_envoyer, 0, sizeof(struct Message));
                            memset(&msg_recu, 0, sizeof(struct Message));
                            continue;
                        }
                        else
                        { // sinon je le rajoute à la queue
                            printf("Je l'ajoute à la queue\n");
                            arg->queue_request[k] = true;
                            // je clear
                            memset(&msg_recu, 0, sizeof(struct Message));
                            continue;
                        }
                    }
                }
                else if (df == dsCv[k])
                {
                    // create une variable pour le message reçu
                    struct Message message_recu_rec;
                    int reception_REPLY;
                    while ((reception_REPLY = recv(dsCv[k], &message_recu_rec, sizeof(struct Message), 0)) == -1) // tant que je peux pas recevoir je continue à réessayer
                    {
                        sleep(1);
                    }
                    if (message_recu_rec.is_reply == true && message_recu_rec.is_release == false)
                    { // si c'est une réponse de type REPLY (je suis prioritaire)
                        pthread_mutex_lock(arg->mutex_var_partagees);
                        arg->varCommune->rep_attendues++;
                        pthread_mutex_unlock(arg->mutex_var_partagees);
                        printf("REPLY reçu du : %i\n", message_recu_rec.sender_id);
                        //printf("mes rep attendues : %i\n", arg->varCommune->rep_attendues);
                        pthread_mutex_lock(arg->mutex_nombre);
                        nombre++; // j'incrémente nombre
                        pthread_mutex_unlock(arg->mutex_nombre);
                        memset(&message_recu_rec, 0, sizeof(struct Message));
                        FD_CLR(df, &set); // une fois j'ai fini je l'enlève de l'ensemble fd_set à scruter
                        continue;
                    }
                    else if (message_recu_rec.is_reply == true && message_recu_rec.is_release == true)
                    { // si c'est une réponse de type RELEASE après que mon voisin est sortie de la SC
                        pthread_mutex_lock(arg->mutex_var_partagees);
                        arg->varCommune->colors[k] = message_recu_rec.couleur;
                        arg->varCommune->rep_attendues++;
                        pthread_mutex_unlock(arg->mutex_var_partagees);
                        printf("RELEASE reçu du : %i || sa couleur est : %i\n", message_recu_rec.sender_id, arg->varCommune->colors[k]);
                        //printf("mes rep attendues : %i\n", arg->varCommune->rep_attendues);
                        pthread_mutex_lock(arg->mutex_nombre);
                        nombre++; // j'incrémente nombre
                        pthread_mutex_unlock(arg->mutex_nombre);
                        memset(&message_recu_rec, 0, sizeof(struct Message));
                        FD_CLR(df, &set); // une fois j'ai fini je l'enlève de l'ensemble fd_set à scruter
                        continue;
                    }
                }
            }
            pthread_mutex_lock(arg->mutex_nombre);
            k++;
            k = k % arg->nombre_voisins_qui_vont_se_connecter_a_moi;
            pthread_mutex_unlock(arg->mutex_nombre);
        }
        pthread_mutex_unlock(arg->mutex_fonction);
        pthread_mutex_lock(arg->mutex_liberation);
        while (arg->varCommune->rep_attendues != arg->nombre_voisins) // tant que j'ai pas toutes les réponses je rentre pas en SC je me mets en attente
        {
            sleep(1);
        }
        // SECTION CRITIQUE
        if (arg->varCommune->rep_attendues == arg->nombre_voisins && !arg->varCommune->isRentre) // si j'ai toutes les réponses et que je suis pas déjà rentré coté demande (c'est à ça que ça sert la variable partagée isRentre)
        {
            pthread_mutex_unlock(arg->mutex_liberation);
            pthread_mutex_lock(arg->mutex_section_critique);
            pthread_mutex_lock(arg->mutex_var_partagees);
            arg->varCommune->couleur = acquisition(arg); // je m'attribue une couleur
            printf("JE SUIS DANS LA SECTION CRITIQUE\n");
            printf("\x1B[3%im",arg->varCommune->couleur);
            printf("MA COULEUR EST : %i\n", arg->varCommune->couleur);
            printf("\x1B[0m");
            arg->varCommune->isRentre = true; // je le mets à vrai comme ça coté demande je rentre pas
            pthread_mutex_unlock(arg->mutex_var_partagees);
            FILE *csvFile;
            csvFile = fopen("colors.csv", "a"); // ouvrir le fichier colors.csv en mode append
            if (csvFile == NULL)
            {
                perror("Error opening file");
                exit(1);
            }
            // printer mon indice + la couleur choisie dans une nouvelle ligne du fichier csv
            fprintf(csvFile, "%d,%d\n", arg->my_id, arg->varCommune->couleur);
            // close le file
            fclose(csvFile);
            pthread_mutex_unlock(arg->mutex_section_critique);
        }
        else
        {
            pthread_mutex_unlock(arg->mutex_liberation);
        }
        // LBIERATION
        pthread_mutex_lock(arg->mutex_fonction);
        arg->demandeur = false;
        for (int i = 0; i < arg->nombre_voisins_qui_vont_se_connecter_a_moi; i++)
        {
            if (arg->queue_request[i] == true) // s'il est dans ma file d'attente je lui envoie un RELEASE
            {
                struct Message message_a_envoyer_liber_rec;
                message_a_envoyer_liber_rec.sender_id = arg->my_id;
                message_a_envoyer_liber_rec.is_reply = true;
                message_a_envoyer_liber_rec.couleur = arg->varCommune->couleur;
                message_a_envoyer_liber_rec.is_release = true;
                int envoi_REPLY;
                while ((envoi_REPLY = send(dsCv[i], &message_a_envoyer_liber_rec, sizeof(struct Message), 0)) == -1)
                {
                    sleep(1);
                }
                printf("RELEASE envoyé au %i\n", arg->varCommune->indice_voisins[i]);
                arg->queue_request[i] = false; // je l'enlève de la queue
                // je clear
                memset(&message_a_envoyer_liber_rec, 0, sizeof(struct Message));
            }
        }
        pthread_mutex_unlock(arg->mutex_fonction);
        close(socketServer); // je close ma socket
    }
    else
    {
        pthread_mutex_unlock(arg->mutex_fonction);
    }
    pthread_exit(NULL);
}

void *demande(void *params1) // client
{
    struct node *args = (struct node *)params1;
    // on protège les variables partagées
    pthread_mutex_lock(args->mutex_fonction);
    if (!args->nombre_voisins_auxquels_je_me_connecte == 0)
    {

        int *socketClient = (int *)malloc((args->nombre_voisins_auxquels_je_me_connecte) * sizeof(int));
        int i = 0;
        int j = args->nombre_voisins_qui_vont_se_connecter_a_moi;
        int valeur = 0;                                          // elle sert pour le df max (taille max du set)
        while (i < args->nombre_voisins_auxquels_je_me_connecte) // tant que je me suis pas connecté à tous mes voisins SERVEUR, je continue à le faire
        {
            // creation de la socket pour la demande
            socketClient[i] = socket(PF_INET, SOCK_STREAM, 0);
            valeur = fmax(socketClient[i], socketClient[i - 1]);
            if (i == 0)
                valeur = socketClient[i];
            // verifier la bonne création
            if (socketClient[i] == -1)
            {
                perror("Client : problème dans la creation de socket client :");
            }
            struct sockaddr_in sockServ;
            sockServ.sin_family = AF_INET;
            sockServ.sin_addr.s_addr = args->voisins[i].sin_addr.s_addr;
            sockServ.sin_port = args->voisins[i].sin_port;
            socklen_t lgA = sizeof(struct sockaddr_in);
            // demande de connexion
            int res;
            while ((res = connect(socketClient[i], (struct sockaddr *)&sockServ, lgA)) == -1)
            {
                sleep(1);
            }
            /* Etape 4 :Envoyer un message au serveur */
            int envoi;
            envoi = send(socketClient[i], &args->my_id, sizeof(int), 0);
            if (envoi == -1)
            {
                perror("Client : probleme lors de l'envoi du message\n ");
            }
            else if (envoi == 0)
            {
                perror("Client : la socket a été fermée \n ");
            }

            pthread_mutex_lock(args->mutex_var_partagees);
            int indiceServeur = ntohs(args->voisins[i].sin_port) - 3000;
            printf("Connexion réussie avec %i\n ",indiceServeur);
            args->varCommune->indice_voisins[j] = indiceServeur;
            pthread_mutex_unlock(args->mutex_var_partagees);

            int reception_REQUEST;
            struct Message msg_recu;
            // je reçois la demande pour rentrer dans la section critique de mon voisin SERVEUR
            while (reception_REQUEST = recv(socketClient[i], &msg_recu, sizeof(struct Message), 0) == -1)
            {
                reception_REQUEST = recv(socketClient[i], &msg_recu, sizeof(struct Message), 0) == -1;
            }
            printf("REQUEST reçue du : %i\n", msg_recu.sender_id);
            //  Incrémenter mon horloge et envoyer un message REQUETE à tous les voisins
            time_t currentTime;
            args->horloge = time(&currentTime);
            args->horloge++;
            args->heure_demande = args->horloge;
            args->demandeur = true;
            // le message à envoyer
            struct Message message_a_envoyer;
            message_a_envoyer.sender_id = args->my_id;
            message_a_envoyer.is_request = true;
            message_a_envoyer.is_reply = false;
            message_a_envoyer.timestamp = args->heure_demande;
            // je lui envoie aussi ma demande de sect critique
            int envoi_REQUEST = send(socketClient[i], &message_a_envoyer, sizeof(struct Message), 0);
            if (envoi_REQUEST == -1)
            {
                perror("Client : probleme lors de l'envoi du message\n ");
                exit(1);
            }
            else if (envoi_REQUEST == 0)
            {
                perror("Client : la socket a été fermée \n ");
                exit(1);
            }

            printf("REQUEST envoyée au %i\n", msg_recu.sender_id);

            args->horloge = fmax(args->horloge, msg_recu.timestamp);
            // si je suis pas prioritaire je lui envoie une réponse REPLY pour qu'il rentre dans la sec crit
            if (!args->demandeur || (args->heure_demande > msg_recu.timestamp) || ((args->heure_demande == msg_recu.timestamp) && (args->my_id > msg_recu.sender_id)))
            {
                message_a_envoyer.sender_id = args->my_id;
                message_a_envoyer.is_request = false;
                message_a_envoyer.is_reply = true;

                int envoi_REPLY = send(socketClient[i], &message_a_envoyer, sizeof(struct Message), 0);
                if (envoi_REPLY == -1)
                {
                    perror("Client : probleme lors de l'envoi du message REPLY\n ");
                    exit(1);
                }
                else if (envoi_REPLY == 0)
                {
                    perror("Client : la socket a été fermée \n ");
                    exit(1);
                }
                message_a_envoyer.sender_id = 0;
                message_a_envoyer.is_request = false;
                message_a_envoyer.is_reply = false;
                message_a_envoyer.timestamp = 0;
                message_a_envoyer.couleur = 0;
                message_a_envoyer.is_release = false;
                printf("REPLY envoyé au %i\n", msg_recu.sender_id);
                // clear le message reçu
                memset(&msg_recu, 0, sizeof(struct Message));
                i++;
                j++;
                continue;
            }
            else
            { // sinon je le rajoute dans la file d'attente
                args->queue_request[i] = true;
                printf("J'ajoute %i à la queue\n",msg_recu.sender_id);
                // je clear
                memset(&msg_recu, 0, sizeof(struct Message));
                i++;
                j++;
                continue;
            }
            i++;
            i = i % args->nombre_voisins_auxquels_je_me_connecte;
        }
        fd_set set2, settmp2; // set contiendra toutes les sockets à scruter, settmp est une copie de set modifiable par select
        FD_ZERO(&set2);       /// Initialise à faux les elements de l'ensemble pointé par set.
        int max = valeur;
        int e;
        for (int i = 0; i < args->nombre_voisins_auxquels_je_me_connecte; i++)
        {
            FD_SET(socketClient[i], &set2); // Ajoute le descripteur socketServer à la liste des descripteurs de *set à scruter, i.e. positionne l'éléement à l'indice desc à vrai.
        }
        int nb_voisins_auxquels_je_me_connecte = args->nombre_voisins_auxquels_je_me_connecte;
        j = args->nombre_voisins_qui_vont_se_connecter_a_moi;
        pthread_mutex_unlock(args->mutex_fonction);
        int cpt = 0;
        // tant que j'ai pas recu toutes les REPLY (cpt il sert à ça une fois que j'ai une réponse ou RELEASE je l'incrémente)
        while (cpt < args->nombre_voisins_auxquels_je_me_connecte)
        {
            settmp2 = set2;
            // je scrute en lecture, rien n'empeche de le faire aussi en ecriture
            e = select(max + 1,  // descripteur max à scruter +1
                       &settmp2, // descripteurs à scruter en entree (readDescs)
                       NULL,     // descripteurs à scruter en sortie (writeDescs)
                       NULL,     // pour scruter des exceptions
                       NULL);    // délais d'attente ou NULL // Si le paramètre timeout est NULL, la fonction attend jusqu'à l'occurrence d'un événement,

            if (e == -1)
            {
                perror("erreur de select\n");
            }
            else if (e == 0)
            {
                printf("y a pas de mise à jr\n");
            }
            else
            {
                for (int i = 0; i < args->nombre_voisins_auxquels_je_me_connecte; i++)
                {
                    // create separate variables for each client's response
                    struct Message message_recu;
                    int reception_REPLY;
                    while ((reception_REPLY = recv(socketClient[i], &message_recu, sizeof(struct Message), 0)) == -1)
                    {
                        reception_REPLY = recv(socketClient[i], &message_recu, sizeof(struct Message), 0);
                    }
                    if (message_recu.is_reply == true && message_recu.is_release == false)
                    {          // si je reçois une réponse d'un voisin j'incrémente rep_attendues
                        cpt++; // j'incrémentee cpt
                        pthread_mutex_lock(args->mutex_var_partagees);
                        args->varCommune->rep_attendues++;
                        pthread_mutex_unlock(args->mutex_var_partagees);
                        printf("REPLY reçu du : %i\n", message_recu.sender_id);
                        //printf("mes rep attendues : %i\n", args->varCommune->rep_attendues);
                        memset(&message_recu, 0, sizeof(struct Message));
                        FD_CLR(socketClient[i], &set2); // une fois j'ai fini je l'enlève de l'ensemble fd_set à scruter
                        continue;
                    }
                    else if (message_recu.is_reply == true && message_recu.is_release == true)
                    {          // si je reçois une réponse après la libération (release) d'un voisin
                        cpt++; // j'incrémentee cpt
                        pthread_mutex_lock(args->mutex_var_partagees);
                        args->varCommune->colors[j] = message_recu.couleur;
                        printf("RELEASE reçu du : %i || sa couleur est : %i\n", message_recu.sender_id, args->varCommune->colors[j]);
                        j++;
                        args->varCommune->rep_attendues++;
                        pthread_mutex_unlock(args->mutex_var_partagees);
                        //printf("mes rep attendues : %i\n", args->varCommune->rep_attendues);
                        memset(&message_recu, 0, sizeof(struct Message));
                        FD_CLR(socketClient[i], &set2); // une fois j'ai fini je l'enlève de l'ensemble fd_set à scruter
                        continue;
                    }
                }
            }
        }
        pthread_mutex_lock(args->mutex_liberation);
        while (args->varCommune->rep_attendues != args->nombre_voisins) // tant que j'ai pas toutes les réponses je rentre pas en SC je me mets en attente
        {
            sleep(1);
        }
        // SECTION CRITIQUE
        if (args->varCommune->rep_attendues == args->nombre_voisins && !args->varCommune->isRentre) // si j'ai toutes les réponses et que je suis pas déjà rentré coté reception (c'est à ça que ça sert la variable partagée isRentre)
        {
            pthread_mutex_unlock(args->mutex_liberation);
            pthread_mutex_lock(args->mutex_section_critique);
            pthread_mutex_lock(args->mutex_var_partagees);
            args->varCommune->couleur = acquisition(args); // je m'attribue une couleur
            printf("JE SUIS DANS LA SECTION CRITIQUE\n");
            printf("\x1B[3%im",args->varCommune->couleur);
            printf("MA COULEUR EST : %i\n", args->varCommune->couleur);
            printf("\x1B[0m");
            args->varCommune->isRentre = true; // je le mets à vrai comme ça coté reception je rentre pas
            pthread_mutex_unlock(args->mutex_var_partagees);
            FILE *csvFile;
            csvFile = fopen("colors.csv", "a"); // ouvrir le fichier colors.csv en mode append
            if (csvFile == NULL)
            {
                perror("Error opening file");
                exit(1);
            }
            // printer mon indice + la couleur choisie dans une nouvelle ligne du fichier csv
            fprintf(csvFile, "%d,%d\n", args->my_id, args->varCommune->couleur);
            // close le file
            fclose(csvFile);
            pthread_mutex_unlock(args->mutex_section_critique);
        }
        else
        {
            pthread_mutex_unlock(args->mutex_liberation);
        }

        // LIBERATION
        pthread_mutex_lock(args->mutex_fonction);
        args->demandeur = false;
        j = args->nombre_voisins_qui_vont_se_connecter_a_moi;
        for (int i = 0; i < args->nombre_voisins_auxquels_je_me_connecte; i++)
        {
            if (args->queue_request[i] == true) // s'il est dans ma file d'attente je lui envoie un RELEASE
            {
                struct Message message_a_envoyer_liber_dem;
                message_a_envoyer_liber_dem.sender_id = args->my_id;
                message_a_envoyer_liber_dem.is_reply = true;
                message_a_envoyer_liber_dem.couleur = args->varCommune->couleur;
                message_a_envoyer_liber_dem.is_release = true;
                int envoi_REPLY_demande;
                while ((envoi_REPLY_demande = send(socketClient[i], &message_a_envoyer_liber_dem, sizeof(struct Message), 0)) == -1) // tant que j'ai pas réussi à envoyer je renvoie
                {
                    sleep(1);
                }
                printf("RELEASE envoyé au %i\n", args->varCommune->indice_voisins[j]);
                args->queue_request[i] = false; // je l'enlève de la queue
                memset(&message_a_envoyer_liber_dem, 0, sizeof(struct Message));
                j++;
            }
            else
            {
                j++;
            }
        }
        for (int i = 0; i < args->nombre_voisins_auxquels_je_me_connecte; i++) // je close les sockets
        {
            close(socketClient[i]);
        }
        pthread_mutex_unlock(args->mutex_fonction);
    }
    else
    {
        pthread_mutex_unlock(args->mutex_fonction);
    }
    pthread_exit(NULL);
}

#endif
