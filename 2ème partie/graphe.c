#include "fonctions.h"

int main(int argc, char *argv[])
{
    // Verifier la bonne utilisation du programme ./client adresse_ip_serveur port_serveur port_client
    if (argc != 6)
    {
        printf("utilisation : %s ip_serveur port_serveur ton_indice ton_port ton_ip\n", argv[0]);
        exit(1);
    }
    /*==========================================COMMUNICATION AVEC SERVEUR CENTRAL ============================================================*/
    printf("*******Echange avec le serveur annuaire*******\n");
    /* Etape 1 : créer une socket avec le type stream utilise le protocole UDP */
    int socketClient = socket(PF_INET, SOCK_DGRAM, 0);

    // verifier la bonne création
    if (socketClient == -1)
    {
        perror("Client : problème dans la creation de socket client :");
        exit(1);
    }

    /* Etape 2 : Nommer la socket du client */
    struct sockaddr_in ad;
    ad.sin_family = AF_INET;
    inet_pton(AF_INET, argv[5], &(ad.sin_addr));
    ad.sin_port = htons((short)atoi(argv[4]));
    int nom = bind(socketClient, (struct sockaddr *)&ad, sizeof(ad));

    if (nom == -1)
    {
        perror("Client : erreur au nommage de la socket \n");
        exit(1);
    }

    /* Etape 3 : Désigner la socket du serveur */
    struct sockaddr_in sockServ;
    sockServ.sin_family = AF_INET;
    inet_pton(AF_INET, argv[1], &(sockServ.sin_addr));
    sockServ.sin_port = htons((short)atoi(argv[2]));
    socklen_t lgA = sizeof(struct sockaddr_in);

    /* Etape 4 :Envoyer un message au serveur */

    int message = atoi(argv[3]);
    int envoi = sendto(socketClient, &message, sizeof(int), 0, (struct sockaddr *)&sockServ, lgA);
    if (envoi == -1)
    {
        perror("Client : probleme lors de l'envoi du message\n ");
        exit(1);
    }
    else if (envoi == 0)
    {
        perror("Client : la socket a été fermée \n ");
        exit(1);
    }
    printf("Noeud %i: adresse envoyée au serveur annuaire\n ", atoi(argv[3]));

    int sommets = 0;

    int receptionnbSommets = recvfrom(socketClient, &sommets, sizeof(int), 0, (struct sockaddr *)&sockServ, &lgA);
    while (receptionnbSommets == -1)
    {
        perror("Client : probleme lors de la réception du message\n ");
        exit(1);
    }
    int taille = 0;
    int receptionTaille = recvfrom(socketClient, &taille, sizeof(int), 0, (struct sockaddr *)&sockServ, &lgA);
    while (receptionTaille == -1)
    {
        perror("Client : probleme lors de la réception du message\n ");
        exit(1);
    }
    struct sockaddr_in voisins[taille];

    for (int i = 0; i < taille; i++)
    {
        int receptionTableau = recvfrom(socketClient, &voisins[i], sizeof(struct sockaddr_in), 0, (struct sockaddr *)&sockServ, &lgA);
        while (receptionTableau == -1)
        {
            perror("Client : probleme lors de la réception du message\n ");
            exit(1);
        }
    }
    int cpt = 0;
    int receptionNbClients = recvfrom(socketClient, &cpt, sizeof(int), 0, (struct sockaddr *)&sockServ, &lgA);
    while (receptionNbClients == -1)
    {
        perror("Client : probleme lors de la réception du message\n ");
        exit(1);
    }

    int nombre_voisins = taille + cpt; // Nombre de tous les voisins

    /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
    close(socketClient);
    // shutdown(socketClient,0);
    printf("****************Fin d'échange****************\n");
    //----------------------------------------------------------------Quand je suis serveur je me mets en écoute-----------------------------------------------------------------------

    //------------------------------------------- Quand je suis client---------------------------------------------------------------------------------------------

    // je crée mon tableau dynamique des indices de tous mes voisins
    int *indiceDesVoisins = (int *)malloc(nombre_voisins * sizeof(int));
    //------------------------------------------- Quand je suis serveur j'accepte et je reçois---------------------------------------------------------------------------------------------
    for (int j = 0; j < nombre_voisins; j++)
    {
        indiceDesVoisins[j] = 0;
    }
    // allocation memoire et initialisation des 2 structures node et nodde chacune pour un thread + struct variable commune qui est commune pour les 2

    struct node *node = malloc(sizeof(struct node));
    struct Message msg_req;
    msg_req.is_reply = false;
    msg_req.is_request = false;
    msg_req.is_release = false;
    msg_req.timestamp = 0;
    msg_req.couleur = 0;
    msg_req.sender_id = atoi(argv[3]);
    node->message_a_envoyer = &msg_req;
    node->nombre_voisins_auxquels_je_me_connecte = taille;
    node->nombre_voisins = nombre_voisins;
    node->voisins = malloc(taille * sizeof(struct sockaddr_in));
    node->queue_request = malloc(cpt * sizeof(bool));
    for (int j = 0; j < cpt; j++)
    {
        node->queue_request[j] = false;
    }
    struct variableCommune varCom;
    varCom.rep_attendues = 0;
    varCom.colors = malloc(nombre_voisins * sizeof(int));
    varCom.indice_voisins = malloc(nombre_voisins * sizeof(int));
    varCom.indice_voisins = indiceDesVoisins;
    varCom.isRentre = false;
    varCom.couleur = 0;

    for (int j = 0; j < nombre_voisins; j++)
    {
        varCom.colors[j] = 0;
    }
    node->varCommune = &varCom;

    for (int j = 0; j < taille; j++)
    {
        node->voisins[j] = voisins[j];
    }
    node->my_id = atoi(argv[3]);
    node->horloge = 0;
    node->demandeur = false;
    node->heure_demande = 0;

    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex, NULL);
    node->mutex_fonction = &mutex;
    pthread_mutex_t mutex_nombre;
    pthread_mutex_init(&mutex_nombre, NULL);
    node->mutex_nombre = &mutex_nombre;

    pthread_mutex_t mutex2;
    pthread_mutex_init(&mutex2, NULL);
    node->mutex_var_partagees = &mutex2;
    pthread_mutex_t mutex_liber;
    pthread_mutex_init(&mutex_liber, NULL);
    node->mutex_liberation = &mutex_liber;
    pthread_mutex_t mutex_crit;
    pthread_mutex_init(&mutex_crit, NULL);
    node->mutex_section_critique = &mutex_crit;

    node->nombre_voisins_qui_vont_se_connecter_a_moi = cpt;
    node->mon_adresseIP = argv[5];
    node->mon_port = (short)atoi(argv[4]);

    struct node *nodde = malloc(sizeof(struct node));
    struct Message msg_reqq;
    msg_reqq.is_reply = false;
    msg_reqq.is_request = false;
    msg_reqq.is_release = false;
    msg_reqq.timestamp = 0;
    msg_reqq.couleur = 0;
    msg_reqq.sender_id = atoi(argv[3]);
    nodde->message_a_envoyer = &msg_reqq;

    nodde->nombre_voisins_auxquels_je_me_connecte = taille;
    nodde->nombre_voisins = nombre_voisins;
    nodde->voisins = malloc(taille * sizeof(struct sockaddr_in));
    nodde->queue_request = malloc(taille * sizeof(bool));
    for (int j = 0; j < taille; j++)
    {
        nodde->queue_request[j] = false;
    }
    for (int j = 0; j < taille; j++)
    {
        nodde->voisins[j] = voisins[j];
    }
    nodde->varCommune = &varCom;
    nodde->my_id = atoi(argv[3]);
    nodde->horloge = 0;
    nodde->demandeur = false;
    nodde->heure_demande = 0;
    pthread_mutex_t mutexx;
    pthread_mutex_init(&mutexx, NULL);
    nodde->mutex_fonction = &mutexx;

    pthread_mutex_t mutexx_liber;
    pthread_mutex_init(&mutexx_liber, NULL);
    nodde->mutex_liberation = &mutexx_liber;
    nodde->mutex_section_critique = &mutex_crit;
    nodde->mutex_var_partagees = &mutex2;
    nodde->nombre_voisins_qui_vont_se_connecter_a_moi = cpt;
    nodde->mon_adresseIP = argv[5];
    nodde->mon_port = (short)atoi(argv[4]);

    // création des threads
    pthread_t thread_reception; // je déclare mon thread
    if (pthread_create(&thread_reception, NULL, reception, node) != 0)
    {
        perror("erreur creation thread");
        exit(1);
    }
    pthread_t thread_demande; // je déclare mon thread
    if (pthread_create(&thread_demande, NULL, demande, nodde) != 0)
    {
        perror("erreur creation thread\n");
        exit(1);
    }
    pthread_join(thread_reception, NULL); // j'attends la fin de son exécution
    pthread_join(thread_demande, NULL);   // j'attends la fin de son exécution

    fgetc(stdin);
    //faire les free nécessaires
    free(node);
    free(nodde);
    free(node->voisins);
    free(nodde->voisins);
    free(node->varCommune->indice_voisins);
    free(node->varCommune->colors);
    free(node->queue_request);
    free(nodde->queue_request);
}
