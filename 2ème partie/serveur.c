#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>

int main(int argc, char *argv[])
{
  int res;
  if (argc != 4)
  {
    printf("utilisation : %s adresseIP_serveur nomDuFichier port_serveur\n", argv[0]);
    exit(1);
  }
  FILE *fichier;
  char ligne[500];

  char EdgeStr[10];
  int sommets;
  int aretes;
  int sommet1;
  int sommet2;

  fichier = fopen(argv[2], "r");
  if (fichier == NULL)
    return 1;

  char lineType = 'c';
  // lire les lignes de commentaires jusqu'au premier 'p'
  while (lineType == 'c')
  {
    fgets(ligne, 500, fichier); //
    lineType = ligne[0];
  }
  // récupération des paramètres de la ligne 'p'
  if (ligne[0] == 'p')
  { //ça sert surtout à récupérer le nombre de sommets pour pouvoir exécuter le programme graphe nbSommets fois
    sscanf(ligne, "%c %s %d %d\n", &lineType, EdgeStr, &sommets, &aretes);
    printf("%c %s %d %d\n", lineType, EdgeStr, sommets, aretes);
  }

  /* Etape 1 : créer une socket */
  int socketServer = socket(PF_INET, SOCK_DGRAM, 0);

  if (socketServer == -1)
  {
    perror("Serveur : pb creation socket :");
    exit(1);
  }

  printf("Serveur : creation de la socket réussie ,appuyer sur entrée pour passer au nommage \n");

  /* Etape 2 : Nommer la socket du seveur */
  struct sockaddr_in addrServer;
  addrServer.sin_family = AF_INET;
  addrServer.sin_addr.s_addr = INADDR_ANY; // on utilise l'adresse locale à mon pc car je fais des tests  en interne
  addrServer.sin_port = htons((short)atoi(argv[3]));

  res = bind(socketServer, (const struct sockaddr *)&addrServer, sizeof(addrServer));

  if (res == -1)
  {
    perror("Serveur :problème lors du nommage de la socket\n ");
    exit(1);
  }

  printf("Socket bien nommée\n");

  struct sockaddr_in adClient;
  socklen_t lgA = sizeof(adClient);
  int dsClient;

  int message;
  struct sockaddr_in tabProc[sommets];
  int i = 0;

  while (i < sommets)
  {
    /* Etape 3 : recevoir un message du client*/
    int reception = recvfrom(socketServer, &message, sizeof(int), 0, (struct sockaddr *)&adClient, &lgA);
    if (reception == -1)
    {
      perror("Serveur :erreur lors de la réception du message\n ");
      // exit(1);
    }
    // tableau qui contient les sockets
    tabProc[message] = adClient;
    printf("j'ai stocké ta socket dans la case %d du tableau \n", message);
    // printf("ton adresse que je vais transmettre à ton prochain est : %s\n", inet_ntoa(tabProc[message].sin_addr));
    i++;
  }

  // je crée mon tableau dynamique de 2 dimensions et de taille aretes/2
  int **table = (int **)malloc((aretes / 2) * sizeof(int *));
  for (int i = 0; i < aretes / 2; i++)
  {
    table[i] = (int *)malloc(2 * sizeof(int));
  }

  // pour chaque ligne du tableau
  for (int i = 0; i < aretes / 2; i++)
  {
    // je lis le sommet gauche et à droite et je les stocke
    fscanf(fichier, "%c %d %d\n", &lineType, &sommet1, &sommet2);
    table[i][0] = sommet1;
    table[i][1] = sommet2;
  }

  for (int i = 1; i < (sommets + 1); i++)
  {
    int cpt = 0;
    int k = 0;
    struct sockaddr_in *voisins = (struct sockaddr_in *)malloc((sommets - 1) * sizeof(struct sockaddr_in));
    for (int j = 0; j < aretes / 2; j++)
    {
      if (table[j][1] == i)
      {
        voisins[k] = tabProc[table[j][0]];
        k++;
      }
      else if (table[j][0] == i)
      {
        cpt++;
      }
    }

    int envoinbSommets = sendto(socketServer, &sommets, sizeof(int), 0, (struct sockaddr *)&tabProc[i], lgA);

    if (envoinbSommets == -1)
    {
      perror("Serveur : probleme lors de l'envoi du message\n ");
      // exit(1);
    }
    else if (envoinbSommets == 0)
    {
      perror("Serveur : la socket a été fermé \n ");
      // exit(1);
    }
    printf("le nbr de sommets est bien envoyé! \n");
   
    int taille = k;
    int envoiTaille = sendto(socketServer, &taille, sizeof(int), 0, (struct sockaddr *)&tabProc[i], lgA);

    if (envoiTaille == -1)
    {
      perror("Serveur : probleme lors de l'envoi du message\n ");
      // exit(1);
    }
    else if (envoiTaille == 0)
    {
      perror("Serveur : la socket a été fermé \n ");
      // exit(1);
    }

    printf("la taille est bien envoyée! \n");

    for (int h = 0; h < k; h++)
    {
      int envoi = sendto(socketServer, &voisins[h], sizeof(struct sockaddr_in), 0, (struct sockaddr *)&tabProc[i], lgA);

      if (envoi == -1)
      {
        perror("Serveur : probleme lors de l'envoi du message\n ");
        // exit(1);
      }
      else if (envoi == 0)
      {
        perror("Serveur : la socket a été fermé \n ");
        // exit(1);
      }

      printf("message bien envoyé! \n");
    }
    int envoi2 = sendto(socketServer, &cpt, sizeof(int), 0, (struct sockaddr *)&tabProc[i], lgA);

    if (envoi2 == -1)
    {
      perror("Serveur : probleme lors de l'envoi du message\n ");
      // exit(1);
    }
    else if (envoi2 == 0)
    {
      perror("Serveur : la socket a été fermé \n ");
      // exit(1);
    }

    printf("message bien envoyé! \n");

    free(voisins);
  }
  close(socketServer);

  printf("Serveur central : je termine\n");
  fclose(fichier);
  free(table);
}