# Projet Programmation Répartie HAI721I


### Structure
 ### partie 1 : 
**graphe.c** : algorthme de mise en relation d'un noeud avec ses voisins'./graphe ip_serveur indiceCLient  portClient Ipclient\n'
**init.c** : Facilite le lancement de plusieurs sites : `./init  nomDuFichier' réglé pour UBUNTU, utilisable sur Windows via la commande : `cmd.exe /c start cmd.exe /c wsl.exe ./graphe 127.0.0.1 indiceSommet nomDuFichier` à adapter selon votre terminal ligne 45 dans `sprintf()`).
**Serveur** : connait le nombre des sommets et leurs liens, envoie a chaque client des informations necessaires sur ses voisins pour qu'ils puissent s'interconnecter. /serveur IPserveur nomFichier PortServeur .
 -----------------

  ###partie 2 :
**graphe.c** : algorthme de mise en relation d'un noeud avec ses voisins + coloration de chaque sommet de telle sorte qu'il n'ait pas la même couleur que ses voisins'./graphe ip_serveur indiceCLient  portClient Ipclient\n'
**init.c** : Facilite le lancement de plusieurs sites : `./init  nomDuFichier' réglé pour UBUNTU, utilisable sur Windows via la commande : `cmd.exe /c start cmd.exe /c wsl.exe ./graphe 127.0.0.1 indiceSommet nomDuFichier` à adapter selon votre terminal ligne 45 dans `sprintf()`).
**Serveur** : connait le nombre des sommets et leurs liens, envoie a chaque client des informations necessaires sur ses voisins pour qu'ils puissent s'interconnecter. /serveur IPserveur nomFichier PortServeur 
 colors.csv: fichier excel généré après la coloration sommet, qui contient deux colonnes : la première indique l'indice du sommet et la deuxième indique la couleur qu'il s'est attribué aprés la section critique.
**fonctions.h** : un header qui contient toutes les fonctions dont le programme graphe.c a besoin, comme isColorAvailable, acquisition, demande et reception. 
* pour pouvoir exécuter les programmes graphe il faudra mettre le fichier qui contient les instances de notre graphe dans la racine (trouvé via ce lien :http://cedric.cnam.
fr/~porumbed/graphs/)
-----------------


## 1 Projet

### Missions
**L'objectif** de ce projet est de concevoir et réaliser un graphe de connexion et de résoudre le problème de coloriation de sommets dans ce graphe.

-----------------
### Contraintes
**Nombre d'étudiants conseillé :**2.

**Langage :** C/C++

**Temps :** 
 * Rendu de la première partie  : **22 novembre 2022**
 * Rendu de la deuxième partie : **30 décembre 2022**
 * Démonstration du 1er rendu : **02 décembre 2022**
 * Démonstration du 2ème rendu: ** 13 Janvier 2023**
-----------------

## 2 Informations annexes
### Collaborateurs
 * BENDHAMANE Rania
 * HADJOUDJA Bachir
-----------------

### Encadrants
* BOUZIANE Hinde 
* GIROUDEAU Rodolphe

